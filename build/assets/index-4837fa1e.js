var r=Object.defineProperty;var c=(s,e,t)=>e in s?r(s,e,{enumerable:!0,configurable:!0,writable:!0,value:t}):s[e]=t;var o=(s,e,t)=>(c(s,typeof e!="symbol"?e+"":e,t),t);import{i as l,d as a,s as d,x as h,c as p}from"./components-b101436d.js";const i=l`
  :host {
    h4 {
      color: gray;
    }
  }
`,m=()=>a("hello-text",n);class n extends d{render(){return h`
            <h4>Hello, <slot></slot></h4>
            <p>${this.sub}</p>
        `}}o(n,"styles",[...p,i,l`
        // More styles here
    `]);export{n as HelloText,m as default};
