import { css, html, LitElement } from "lit";
import { componentStyles } from "~src/global";
import { defineComponent } from "~utils/components";
import scopedStyles from "./styles.js";

export default () => defineComponent("hello-text", HelloText);
export class HelloText extends LitElement {
    render() {
        return html`
            <h4>Hello, <slot></slot></h4>
            <p>${this.sub}</p>
        `;
    }

    // Styles can either be in this file (only css)
    // or imported from another file (scss in this case)
    static styles = [...componentStyles, scopedStyles, css`
        // More styles here
    `];
}
