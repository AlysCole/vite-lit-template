import { css, html, LitElement } from "lit";
import { pageStyles } from "~src/global";
import { getUsername } from "~services/name-service";
import { defineComponent } from "~utils/components.js";
import scopedStyles from "./styles.js";

import("~components/hello-text").then(f => f.default());

export default class MooPage extends LitElement {
    render() {
        return html`
            <div class="container">
              Moo
            </div>
        `;
    }

    connectedCallback() {
        super.connectedCallback();
        this.username = getUsername();
        // eslint-disable-next-line no-console
        if (!PRODUCTION) console.log("Everything is working!");
    }

    // Styles can either be in this file (only css)
    // or imported from another file (scss in this case)
    static styles = [...pageStyles, scopedStyles, css`
        // More styles here
    `];
}

defineComponent("moo-page", MooPage);
