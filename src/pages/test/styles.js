import { css } from "lit";

/* Do not @import or @use global styles (except for theming) */
/* To avoid css duplication (it's not tree-shakable) */

export default css`
  .container {
    margin: 40px;
  }
`;
