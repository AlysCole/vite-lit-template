import { css } from "lit";

// Applied to all pages (not components)
export default css`
:host {
  width: 100%;
  height: 100%;
}
`;
