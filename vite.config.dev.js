import { defineConfig, mergeConfig } from 'vite';

import common from "./vite.config.common";

export default defineConfig(mergeConfig(common, {
  mode: "development",

  server: {
    hmr: true,
  }
}));
