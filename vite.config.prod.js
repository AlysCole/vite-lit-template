import { defineConfig, mergeConfig } from 'vite';

import common from "./vite.config.common";
import paths from "./vite.paths";

export default defineConfig(mergeConfig({
  mode: "production",

  build: {
    outDir: paths.build,
    minify: 'esbuild',
  }
}, common));

