import * as fs from "fs";
import * as path from "path";
import { defineConfig } from "vite";
import { createMpaPlugin } from 'vite-plugin-virtual-mpa'

import router from "./router";
import paths from "./vite.paths";

const pages = router.map(config => ({
    ...config,
    keyName: config.source.replace(/\//g, "").replace(/-/g, ""),
    component: fs.readFileSync(`${paths.pages}${config.source}/index.js`).toString().match(/defineComponent\("([^"]+)",/)[1],
}));

for (let i = 0; i < pages.length; i++) {
  pages[i].outputPath = pages[i].outputPath === undefined ? pages[i].source : pages[i].outputPath;
}

export default defineConfig({
  root: paths.src,
  base: "/build", 
  plugins: [
    createMpaPlugin({
      template: `${paths.pages}/base.html`,
      pages: pages.map(page => ({
        name: page.keyName,
        filename: page.outputFileName,
        entry: `${paths.pages}${page.source}/index.js`,
        data: {
          title: page.title,
          description: page.description,
          component: page.component
        }
      })),
      previewRewrites: [
        // If there's no index.html, you need to manually set rules for history fallback like:
        { from: /.*/, to: '/index.html' },
      ],
    })
  ],
  resolve: {
    alias: {
      "~services": path.resolve(__dirname, "src/services"),
      "~components": path.resolve(__dirname, "src/components"),
      "~utils": path.resolve(__dirname, "src/utils"),
      "~styles": path.resolve(__dirname, "src/assets/styles"),
      "~src": path.resolve(__dirname, "src"),
    }
  },
});

